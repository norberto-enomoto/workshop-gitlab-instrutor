FROM azul/zulu-openjdk-alpine:11-jre

LABEL author="Norberto Hideaki Enomoto"
LABEL email="norberto.enomoto@dxc.com"
LABEL company="DXC Technology"


RUN addgroup -g 3000 spring-boot-group
RUN adduser -D -g '' -G spring-boot-group -u 1000 spring-boot-user
RUN apk update && apk upgrade
USER spring-boot-user

WORKDIR /home/spring-boot-user
COPY --chown=spring-boot-user:spring-boot-group target/spring-boot-ms-0.0.1-SNAPSHOT.jar /home/spring-boot-user

EXPOSE 8080
ENTRYPOINT ["java","-jar","/home/spring-boot-user/spring-boot-ms-0.0.1-SNAPSHOT.jar"]
